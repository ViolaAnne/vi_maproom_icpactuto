require "functions"

Languages={{"en","es","fr"},{"es","en","fr"},{"ru","en","es","fr"},{"fr","en","es"},{"ar","en","es","fr"},{"id","en","es","fr"}}

RepoDir = RepoDir or os.getenv('PWD')
Host = "digilib.icpac.net"
Port = 8180
ServerAdmin = "webmaster@localhost"
ServerName = "localhost"
ServerAlias = "localhost"
ProxyModule = true -- set to false if you would like to use rewrite rules
ProxyURL = "http://digilib.icpac.net"
MacSetup = false

if ProxyModule then
ProxyRules = [[
   ProxyPass /SOURCES ]]..ProxyURL..[[/SOURCES
   ProxyPass /expert ]]..ProxyURL..[[/expert
   ProxyPass /ds: ]]..ProxyURL..[[/ds:
   ProxyPass /home ]]..ProxyURL..[[/home
   ProxyPass /openrdf-sesame ]]..ProxyURL..[[/openrdf-sesame
   ProxyPass /servicestatus ]]..ProxyURL..[[/servicestatus
]]
else
ProxyRules = [[
   RewriteRule ^/SOURCES/(.*) ]]..ProxyURL..[[/SOURCES/$1
   RewriteRule ^/expert/(.*) ]]..ProxyURL..[[/expert/$1
   RewriteRule ^/ds:/(.*) ]]..ProxyURL..[[/ds:/$1
   RewriteRule ^/home/(.*) ]]..ProxyURL..[[/home/$1
   RewriteRule ^/openrdf-sesame/(.*) ]]..ProxyURL..[[/openrdf-sesame/$1
   RewriteRule ^/servicestatus/ ]]..ProxyURL..[[/servicestatus/
]]
end
